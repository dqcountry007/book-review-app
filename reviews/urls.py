from django.urls import path
#list_reviews create_review review_details are our functions that we import
from reviews.views import list_reviews, create_review, review_detail

urlpatterns = [
    path("", list_reviews, name="reviews_list"),
    #import the create_review function, using it in the path function that 
    #maps it to the "new/" path. 
    path("new/", create_review, name="create_review"),
    path("<int:id>/", review_detail, name="review_detail"),
]

#<int:id> creates the id value from what's in the url path
#if the path is reviews/13/ then Django takes the value 13
#and passes that to the parameter of our view function thats named id. 

#this is where we will configure the view handlers for the
# reviews Django app. the line from django.urls import path will
# import the path function from the django.urls module 

#importing and using the list_reviews function that was defined
#in the views.py file. Since python files are modules, the views.py
#file is the reviews.views module in our Django app. 

#path function from the django.urls module to register that
#list_reviews shoul get called when someone requests this path. 
#gave it a name to refer to it later. 


