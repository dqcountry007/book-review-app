from django.shortcuts import render, redirect

# Create your views here.
#Need a function that will gather data and send the HTML back
#to the browser. 
#Import our Review model because we want to use it get reviews
#from the data base. 
from reviews.models import Review
from reviews.forms import ReviewForm

#list_reviews function fets all the review from database with
#Review.objects.all().
#It then takes that and puts it into a directory stored in a variable
#named context 
#It render the html in the main.html file in the reviews directory
#INSIDE of the templates directory using the data in the context variable
#Once html is generated, it is sent back to browser. 
#Whatever value that comes back from the render function is returned
#from the list_reviews function because thats what Django expects.
def list_reviews(request):
    reviews = Review.objects.all()
    context = {
        "reviews": reviews,
    }
    return render(request, "reviews/main.html", context)
#Declare a function named create_review, which has a single input parameter that contains
#the HTTP request
def create_review(request):
    #create a new review form for the request
    form = ReviewForm()
    #if the request is a POST request, then
    if request.method == "POST":
        #create a new form with the data in the HTTP POST
        form = ReviewForm(request.POST)
        #test to see if the data is valid
        if form.is_valid():
            #since the data is valid, save the data
            form.save()
            #redirect to the main page that contains the list of reviews and return
            #from the function (nothing else after this will run if both if statements are true)
            return redirect("reviews_list")
            #Create a context dictionary
    context = {
        #add the form to the dictionary
        "form": form,
        #CLOSE THE CURLY BRACE!! 
    }
    #render the template and return that to Django 
    return render(request, "reviews/create.html", context)

def review_detail(request, id):
    review = Review.objects.get(id=id)
    context = {
        "review": review,
    }
    return render(request, "reviews/detail.html", context)