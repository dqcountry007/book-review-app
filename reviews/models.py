from django.db import models

# Create your models here.
#defines the class Review for us. Lets us interact with data in database
class Review(models.Model):
    #Creates a property on that review named cover_image that contains a URL
    cover_image = models.URLField()
    #Create properties named title that has a string with a max length
    title = models.CharField(max_length=100)
    #Creates a property named rating that will contain an integer, a small integer
    rating = models.SmallIntegerField()
    #Create property named author that has a string with a max length of 100
    authors = models.CharField(max_length=100)
    #Create properties named summary containing a string with no max length
    summary = models.TextField()
    #Create property named review containing a string with no max length
    review = models.TextField()
    #Creates a property named created that will contain a new kind of data
    #type for us, a date-time value, which specifies a point in time on a certain day
    created = models.DateTimeField()
    

